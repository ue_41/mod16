﻿#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    
    int dayNumber = buf.tm_mday;

    const int m = 5;
    int array[m][m];
  
    int sumArray[m] = { 0 };

    for (int i = 0; i < m; i++)
    {
        
        for (int j = 0; j < m; j++)
        {
            array[i][j] = (i+j);
            std::cout << array[i][j]<< " ";
            
            sumArray[i] += array[i][j];
        }
        std::cout << '\n';
               
    }
    std::cout << std::endl;

    std::cout << "Today is " << dayNumber << std::endl;

    int numb = dayNumber % m;
      
    std::cout << sumArray[numb] << std::endl;
    
}

